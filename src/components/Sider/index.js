import React, { useState } from "react";
import {
  AppstoreOutlined,
  DoubleLeftOutlined,
  DoubleRightOutlined,
  SearchOutlined,
  HomeOutlined,
  CaretUpFilled,
  UserOutlined,
  FolderOpenOutlined,
} from "@ant-design/icons";
import { Logo, RightBracket } from "../../assets/logo/icon";
import { Button, Input, Space, Avatar } from "antd";
import { OuterComponent, SiderMenu } from "./styles";
import { useNavigate, useLocation } from "react-router-dom";

const getCurrentTab = (str, key) => {
  const paths = str && str.split("/");
  return paths && paths[key];
};

export const sidebarMenu = [
  {
    key: "metrix",
    labels: "Metrix",
    Icon: HomeOutlined,
    url: "/metrix",
  },
  {
    key: "bundle",
    labels: "Bundle",
    Icon: FolderOpenOutlined,
    url: "/bundle",
  },
  {
    key: "project",
    labels: "Projects",
    Icon: AppstoreOutlined,
    url: "/project/list",
  },
];

const Sidebar = () => {
  const [collapsed, setCollapsed] = useState(false);
  const navigate = useNavigate();
  const location = useLocation();
  const url = getCurrentTab(location.pathname, 1);

  return (
    <OuterComponent
      breakpoint="md"
      trigger={null}
      collapsible
      collapsed={collapsed}
    >
      <div className="sider-head">
        <Logo />
        <Button
          type="text"
          icon={collapsed ? <DoubleRightOutlined /> : <DoubleLeftOutlined />}
          onClick={() => setCollapsed(!collapsed)}
        />
      </div>
      <Space direction="horizontal" className="textBox">
        <Space>
          <Input
            className="textArea"
            placeholder={!collapsed && "Search order"}
            prefix={<SearchOutlined className="site-form-item-icon" />}
          />
        </Space>
      </Space>

      <SiderMenu
        theme="light"
        mode="inline"
        selectedKeys={[url || "bundle"]}
        defaultSelectedKeys={["url ||bundle"]}
      >
        {sidebarMenu.map((menu) => {
          return (
            <SiderMenu.Item
              key={menu.key}
              title={menu.labels}
              onClick={() => navigate(menu.url)}
            >
              <div className="menuSelected"></div>

              {<menu.Icon />}
              {!collapsed && <span className="menu-label">{menu.labels}</span>}
            </SiderMenu.Item>
          );
        })}
      </SiderMenu>

      <div className="sider-footer">
        <div className="sider-footer-child">
          <Space wrap size={16}>
            <Avatar size="small" icon={<UserOutlined />} />
          </Space>
          <CaretUpFilled />
          <span>{!collapsed && <h5 className="user-name">Huy Nguyen</h5>}</span>
        </div>
        <RightBracket />
      </div>
    </OuterComponent>
  );
};

export default Sidebar;
