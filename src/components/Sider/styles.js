import styled from "styled-components";
import { Layout, Menu } from "antd";
import "../../styles/global.css";

const { Sider } = Layout;
export const OuterComponent = styled(Sider)`
  &.ant-layout-sider {
    background: var(--bg-color);
  }
  .ant-layout-sider-children {
    min-height: 100vh;
    .ant-btn {
      font-size: 16px;
      color: var(--icon-color);
      border-radius: 3px;
      border: 1px solid #edeef2;
      background: var(--bg-color);
      width: 20px;
      height: 20px;
      flex-shrink: 0;
      display: inline-flex;
      align-items: center;
      justify-content: center;
    }
  }
  .ant-space-item > span {
    border: none;
  }
  .textBox {
    margin: 5px 7px 5px 7px;
    border: none;
  }

  .sider-head {
    display: flex;
    padding: 12px 10px;
    justify-content: space-between;
    align-items: center;
  }

  .sider-footer {
    position: absolute;
    display: flex;
    bottom: 0;
    width: 100%;
    background-color: var(--bg-color);
    padding: 0 10px 5px 5px;
    justify-content: space-between;
    align-items: center;
    overflow: hidden;

    .sider-footer-child {
      width: 100%;
      height: 30px;
      display: inline-flex;
      align-items: center;
      gap: 5px;
      background: var(--bg-color);
    }
    .user-name {
      color: var(--primary-color);
      font-weight: 500;
      display: inline-block;
    }
  }
`;
export const SiderMenu = styled(Menu)`
  &.ant-menu-root.ant-menu-inline {
    border-inline-end: none;
  }
  .ant-menu-title-content {
    display: flex;
    align-items: center;
  }
  .ant-menu-item-selected {
    color: var(--primary-color);
    background: var(--bg-input-color);
    .menuSelected {
      position: absolute;
      width: 3px;
      height: 26px;
      border-radius: 5px;
      background: var(--primary, #6852d3);
      left: 10px;
    }
  }
`;
