import { Button, Layout } from "antd";
import React, { useEffect } from "react";
import { CustomContent, TableContainer } from "../../../styles/components";
import { FormOutlined, EyeOutlined, DeleteOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { Link } from "react-router-dom";
import {
  deleteProjectAction,
  getProjectListAction,
} from "../../../redux/projects/action";
import {
  getAllProjects,
  getAllProjectsSelector,
} from "../../../redux/projects/selector";

const { Content } = Layout;

const ProjectListTable = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getProjectListAction());
  }, [dispatch]);
  const dataSource = useSelector(getAllProjectsSelector);

  const handleDelete = (id) => {
    dispatch(deleteProjectAction(id));
  };
  const defaultColumns = [
    {
      title: "",
      dataIndex: "image",
      width: "10%",
      render: (image) => (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "100%",
          }}
        >
          <img
            src={image}
            alt="Image"
            style={{ maxWidth: "70%", maxHeight: "70%" }}
          />
        </div>
      ),
    },
    {
      title: "NAME",
      dataIndex: "projectName",
      width: "12%",
      editable: true,
    },
    {
      title: "STATUS",
      dataIndex: "isActive",
      width: "16%",
      render: (isActive) => (
        <a className="active">{isActive ? "Active" : "Inactive"}</a>
      ),
    },
    {
      title: "REGION",
      dataIndex: "region",
      width: "16%",
    },
    {
      title: "TOTAL ENVIRONMENT",
      width: "17%",
      dataIndex: "configQuantity",
    },
    {
      title: "UPDATE DAY",
      width: "17%",
      dataIndex: "updatedAt",
      render: (updatedAt) => moment(updatedAt).format("DD/MM/yyyy HH:mm A"),
    },
    {
      title: "",
      width: "4%",
      render: (_, record) => (
        <Link to={`/project/detail/${record.id}`}>
          <EyeOutlined />
        </Link>
      ),
    },
    {
      title: "",
      width: "4%",
      render: (_, record) => (
        <div>
          <Link to={`/project/edit/${record.id}`}>
            <FormOutlined />
          </Link>
        </div>
      ),
    },
    {
      title: "",
      width: "4%",
      render: (_, record) => (
        <Button
          className="deleteButton"
          onClick={() => handleDelete(record.id)}
        >
          <DeleteOutlined />
        </Button>
      ),
    },
  ];

  const columns = defaultColumns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => ({
        record,
        editable: col.editable,
        dataIndex: col.dataIndex,
        title: col.title,
      }),
    };
  });

  return (
    <Content style={{ overflow: "auto" }}>
      <CustomContent>
        <TableContainer
          bordered={false}
          dataSource={dataSource}
          columns={columns}
          size="middle"
        />
      </CustomContent>
    </Content>
  );
};
export default ProjectListTable;
