import React, { useEffect } from "react";
import { getBundlesAction } from "../../../redux/bundles/action";
import { Popconfirm } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { Layout } from "antd";
import { CustomContent, TableContainer } from "../../../styles/components";

const { Content } = Layout;
const defaultColumns = [
  {
    title: "ID",
    dataIndex: "id",
    width: "35%",
    editable: true,
  },
  {
    title: "NAME",
    dataIndex: "name",
    width: "10%",
    editable: true,
  },
  {
    title: "STATUS",
    dataIndex: "isActive",
    width: "10%",
    render: (isActive) => (
      <span className="active">{isActive ? "Active" : "Inactive"}</span>
    ),
  },
  {
    title: "SUPPORT",
    dataIndex: "support",
    width: "15%",
  },
  {
    title: "PRICE",
    dataIndex: "price",
  },
  {
    title: "POWER",
    dataIndex: "power",
  },
  {
    title: "TYPE",
    dataIndex: "type",
  },
  {
    title: "CPU",
    dataIndex: "cpu",
  },
  {
    title: "RAM (GB)",
    width: "10%",
    dataIndex: "ram",
  },
  {
    title: "MONTHY TRANSFER",
    width: "30%",
    dataIndex: "monthlyTransfer",
  },
];
const columns = defaultColumns.map((col) => {
  if (!col.editable) {
    return col;
  }
  return {
    ...col,
    onCell: (record) => ({
      record,
      editable: col.editable,
      dataIndex: col.dataIndex,
      title: col.title,
    }),
  };
});

const BundleTable = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getBundlesAction());
  }, [dispatch]);
  const dataSource = useSelector((state) => state.bundles.data);

  return (
    <Content style={{ overflow: "auto" }}>
      <div>
        <CustomContent>
          <TableContainer
            bordered={false}
            dataSource={dataSource}
            columns={columns}
            size="middle"
          />
        </CustomContent>
      </div>
    </Content>
  );
};
export default BundleTable;
