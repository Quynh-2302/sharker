import { Card } from "antd";
import styled from "styled-components";
import "../../../styles/global.css";

export const ConfigCard = styled(Card)`
  min-width: 500px;
  .ant-btn-default:not(:disabled):hover {
    color: var(--bg-color);
  }
  .ant-card-head .ant-tabs-top {
    color: var(--grey-300, #a1b0cc);
  }
`;
