import React, { useEffect } from "react";
import { TableContainer } from "../../../../styles/components";
import { FormOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { getLoadBalanceAction } from "../../../../redux/config/action";
import { Link, useParams } from "react-router-dom";

const defaultColumns = [
  {
    title: "LOAD BALANCE ID",
    dataIndex: "id",
    width: "35%",
    editable: true,
  },
  {
    title: "NAME",
    dataIndex: "name",
    width: "20%",
  },
  {
    title: "STATUS",
    dataIndex: "isActive",
    width: "10%",
    render: (isActive) => (
      <a className="active">{isActive ? "Active" : "Inactive"}</a>
    ),
  },
  {
    title: "PRIVATE PORT",
    width: "15%",
    dataIndex: "sshKey",
  },

  {
    title: "",
    width: "10%",
    render: (_, record) => (
      <div>
        <Link to={`/project/edit/${record.id}`}>
          <FormOutlined />
        </Link>
      </div>
    ),
  },
];
const columns = defaultColumns.map((col) => {
  if (!col.editable) {
    return col;
  }
  return {
    ...col,
    onCell: (record) => ({
      record,
      dataIndex: col.dataIndex,
      title: col.title,
    }),
  };
});

const BalanceTable = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getLoadBalanceAction());
  }, [dispatch]);

  const contentLocalTable = Object.values(
    useSelector((state) => state.configs.LoadBalances)
  );
  return (
    <TableContainer
      bordered={false}
      dataSource={contentLocalTable}
      columns={columns}
      size="middle"
      pagination={false}
    />
  );
};

export default BalanceTable;
