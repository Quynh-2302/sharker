import React, { useState, useEffect } from "react";
import { TableContainer, SaveButton } from "../../../styles/components";
import { PlusOutlined, FormOutlined, DeleteOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteConfigsAction,
  getConfigAction,
  getLoadBalanceAction,
} from "../../../redux/config/action";
import { Link, useParams } from "react-router-dom";
import BalanceTable from "./BalanceTable";
import { Button } from "antd";
import { ConfigCard } from "./styles";
import {
  getAllConfigsSelector,
  getConfigsByProjectId,
} from "../../../redux/config/selector";

const ConfigTable = () => {
  const { id } = useParams();
  const dispatch = useDispatch();

  const handleDelete = (configId) => {
    dispatch(deleteConfigsAction(configId));
  };

  const defaultColumns = [
    {
      title: "NAME",
      dataIndex: "name",
      width: "20%",
    },
    {
      title: "AVAILABILITY ZONE",
      dataIndex: "availabilityZone",
      width: "20%",
      editable: true,
    },
    {
      title: "CURRENT INSTANCE",
      dataIndex: "currentInstance",
      width: "20%",
    },
    {
      title: "STATUS",
      dataIndex: "isActive",
      width: "15%",
      render: (isActive) => (
        <a className="active">{isActive ? "Active" : "Inactive"}</a>
      ),
    },
    {
      title: "PRIVATE PORT",
      width: "15%",
      dataIndex: "privatePort",
    },

    {
      title: "",
      width: "5%",
      render: (_, record) => (
        <div>
          <Link to={`/project/config/edit/${record.id}`}>
            <FormOutlined />
          </Link>
        </div>
      ),
    },
    {
      title: "",
      width: "5%",
      render: (_, record) => (
        <Button
          className="deleteButton"
          onClick={() => handleDelete(record.id)}
        >
          <DeleteOutlined />
        </Button>
      ),
    },
  ];
  const columns = defaultColumns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => ({
        record,
        dataIndex: col.dataIndex,
        title: col.title,
      }),
    };
  });
  const tabList = [
    {
      key: "Config",
      tab: "Config",
    },
    {
      key: "LoadBalance",
      tab: "Load Balance",
    },
  ];
  useEffect(() => {
    dispatch(getConfigAction());
    dispatch(getLoadBalanceAction());
  }, [dispatch]);

  const configDataById = useSelector((state) =>
    getAllConfigsSelector(state, id)
  );
  //card
  const [activeConfig, setactiveConfig] = useState("Config");
  const onConfigChange = (key) => {
    setactiveConfig(key);
  };

  const contentList = {
    Config: (
      <TableContainer
        bordered={false}
        dataSource={configDataById}
        columns={columns}
        size="middle"
        pagination={false}
      />
    ),
    LoadBalance: <BalanceTable />,
  };

  return (
    <>
      <ConfigCard
        tabBarExtraContent={
          <Link to={`/project/config/create/${id}`}>
            <SaveButton>
              <PlusOutlined /> New
            </SaveButton>
          </Link>
        }
        tabList={tabList}
        activeTabKey={activeConfig}
        onTabChange={onConfigChange}
      >
        {contentList[activeConfig]}
      </ConfigCard>
    </>
  );
};

export default ConfigTable;
