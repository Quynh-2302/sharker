import { useEffect } from "react";
import { Col, Row, Radio } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { CheckOutlined } from "@ant-design/icons";
import { ConfigCard, CustomRow } from "../../../pages/Config/Create/styles";
import { getBundlesAction } from "../../../redux/bundles/action";
import { TableButton } from "../../../styles/components";
import {
  getAllBundles,
  getAllBundlesSelector,
} from "../../../redux/bundles/selector";

const BundleCard = ({ selectedItem, handleRadioChange }) => {
  const dispatch = useDispatch();
  //Bundle
  useEffect(() => {
    dispatch(getBundlesAction());
  }, [dispatch]);
  const bundleData = useSelector(getAllBundlesSelector);

  return (
    <>
      {bundleData ? (
        <ConfigCard className="createBundle" title="Bundle">
          <Row className="bundleTitle" gutter={[12, 12]}>
            <Col span={1}></Col>
            <Col span={2}></Col>
            <Col span={2}>Pricing</Col>
            <Col span={4}>Supported flatforms</Col>
            <Col span={2}>CPU Count</Col>
            <Col span={3}>Disk (GB)</Col>
            <Col span={2}>Power</Col>
            <Col span={2}>Ram (GB)</Col>
            <Col span={4}>Monthly transfer (GB)</Col>
            <Col span={2}></Col>
          </Row>

          {bundleData.map((item, index) => (
            <CustomRow
              key={index}
              gutter={[24, 24]}
              className={selectedItem === item ? "selected-row" : ""}
            >
              <Col span={1}>
                <Radio
                  className="radio"
                  checked={item === selectedItem}
                  onChange={() => handleRadioChange(item)}
                />
              </Col>
              <Col span={2}>{item.name}</Col>
              <Col span={2}>{item.price}</Col>
              <Col span={4}>{item.support}</Col>
              <Col span={2}>{item.cpu}</Col>
              <Col span={3}>{item.code}</Col>
              <Col span={2}>{item.power}</Col>
              <Col span={2}>{item.ram}</Col>
              <Col span={4}>{item.monthlyTransfer}</Col>
              <Col span={2} className="selectButton">
                <div
                  selected={selectedItem === item}
                  onClick={() => handleRadioChange(item)}
                >
                  {selectedItem === item ? (
                    <TableButton className="primaryButton">
                      <CheckOutlined />
                      Selected
                    </TableButton>
                  ) : (
                    <TableButton className="secondButton">Select</TableButton>
                  )}
                </div>
              </Col>
            </CustomRow>
          ))}
        </ConfigCard>
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
};
export default BundleCard;
