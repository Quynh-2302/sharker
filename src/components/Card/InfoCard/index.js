import { Button, Col, Row } from "antd";
import React, { useEffect, useState } from "react";
import { InforTable } from "./styles";
import { FormOutlined, DeleteOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { getDetailProjectAction } from "../../../redux/projects/action";
import { Link, useParams } from "react-router-dom";
import { getProjectByIdSelector } from "../../../redux/projects/selector";

const InforCard = () => {
  const { id } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDetailProjectAction(id));
  }, [dispatch, id]);

  const detailProject = useSelector((state) =>
    getProjectByIdSelector(state, { id })
  );
  return (
    <>
      {detailProject ? (
        <InforTable title="Project Detail">
          <Row gutter={[24, 24]}>
            <Col span={2}></Col>
            <Col span={4}>NAME</Col>
            <Col span={5}>STATUS</Col>
            <Col span={6}>REGION</Col>
            <Col span={4}>API Key</Col>
            <Col span={3}></Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={2}>
              <img
                src={detailProject.image}
                alt="Image"
                className="logoImage"
              />
            </Col>
            <Col span={4}>{detailProject.projectName}</Col>
            <Col span={5} className="activeStatus">
              <a>{detailProject.isActive ? "Active" : "Inactive"} </a>
            </Col>
            <Col span={6}>{detailProject.region}</Col>
            <Col span={4}>{detailProject.apiKey}</Col>
            <Col span={2}>
              <Link to={`/project/edit/${id}`}>
                <FormOutlined />
              </Link>
            </Col>
          </Row>
        </InforTable>
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
};

export default InforCard;
