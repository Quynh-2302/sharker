import { styled } from "styled-components";
import "../../../styles/global.css";
import { Card } from "antd";

export const InforTable = styled(Card)`
  margin-bottom: 14px;
  min-width: 500px;
  .ant-row {
    margin-bottom: 12px;
    display: flex;
    align-items: center;
  }
  .logoImage {
    width: 100%;
    height: 100%;
  }
  .ant-card-body {
    padding-bottom: 0px;
  }
  .ant-card-bordered {
    border: none;
  }
  .activeStatus a {
    color: var(--act-color);
    background: var(--bg-act-color);
    border-radius: 3px;
    padding: 2px 5px;
    font-size: 14px;
  }
  a {
    color: var(--primary-color);
    font-weight: 1200;
    font-size: 16px;
  }
`;
