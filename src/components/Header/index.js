import React from "react";
import { SearchOutlined, LeftOutlined } from "@ant-design/icons";
import { Col, Space, Input } from "antd";
import { StyledHeader } from "./styles";
import { Ellipsis } from "../../assets/logo/icon";
import { useNavigate } from "react-router-dom";

const Header = ({ title, buttons }) => {
  const navigate = useNavigate();
  return (
    <StyledHeader>
      <Col span={8}>
        <Space>
          <div className="leftHeader">
            <button className="backButton" onClick={() => navigate(-1)}>
              <LeftOutlined /> Back
            </button>
          </div>
          <div className="headerTitle">{title}</div>
        </Space>
      </Col>
      <Col span={12}>
        <Space direction="horizontal" className="textBox">
          <Input
            placeholder="Search order"
            prefix={<SearchOutlined className="site-form-item-icon" />}
            suffix={<Ellipsis className="site-form-item-icon" />}
          />
        </Space>
      </Col>
      <Col span={4}>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          {buttons &&
            buttons.map((button, index) => (
              <button
                key={index}
                className={button.className}
                onClick={button.onClick}
                title={button.label}
                icon={button.Icon}
              >
                {button.Icon} {button.label}
              </button>
            ))}
        </div>
      </Col>
    </StyledHeader>
  );
};

export default Header;
