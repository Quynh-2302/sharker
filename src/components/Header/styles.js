import { Layout } from "antd";
import styled from "styled-components";
import "../../styles/global.css";

const { Header } = Layout;
export const StyledHeader = styled(Header)`
  padding: 35px 35px 35px 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: var(--bg-color);

  .textBox {
    .test {
      display: flex;
      padding: 12px 10px;
      justify-content: space-between;
      align-items: center;
    }

    .test input {
      margin: 5px 7px;
    }

    .test svg {
      width: 10px;
      height: 3px;
      fill: none;
      path {
        fill: var(--icon-color);
      }
    }
  }
  .headerTitle {
    color: #000;
    font-size: 14px;
    font-style: normal;
    font-weight: 550;
    line-height: normal;
  }
  .ant-space-item > span {
    border: none;
  }
  .backButton {
    display: inline-flex;
    padding: 5px;
    align-items: flex-start;
    gap: 6px;
    border: none;
    border-radius: 5px;
    background: rgba(68, 39, 227, 0.05);
    color: var(--primary-color);
  }
  .primaryButton {
    background: var(--primary-color);
    color: var(--bg-color);
    display: inline-flex;
    height: 28px;
    padding: 5px 10px;
    justify-content: center;
    align-items: center;
    gap: 10px;
    flex-shrink: 0;
    border: none;
    border-radius: 7px;
  }
  .secondButton {
    background: var(--bg-input-color);
    color: var(--primary-color);
    display: inline-flex;
    height: 28px;
    padding: 5px 10px;
    justify-content: center;
    align-items: center;
    gap: 10px;
    flex-shrink: 0;
    border: none;
    border-radius: 7px;
    margin-right: 5px;
  }
  .ant-btn-primary:not(:disabled):hover {
    background: var(--bg-color);
    color: var(--primary-color);
    border: 1px solid var(--primary-color);
  }
`;
