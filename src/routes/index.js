import * as React from "react";
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";
import { useSelector } from "react-redux";
import CreateProject from "../pages/Project/Create";
import ListProject from "../pages/Project/List";
import ProjectDetail from "../pages/Project/Detail";
import Login from "../pages/Login";
import EditProject from "../pages/Project/Edit";
import BundlePage from "../pages/Bundle";
import GeneralLayout from "../layout/GeneralLayout";
import CreateConfig from "../pages/Config/Create";
import EditConfig from "../pages/Config/Detail";

export const Router = () => {
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route path="/" element={isLoggedIn ? <GeneralLayout /> : <Login />}>
        <Route path="bundle" element={<BundlePage />} />
        <Route path="project">
          <Route path="config/create/:id" element={<CreateConfig />} />
          <Route path="config/edit/:id" element={<EditConfig />} />
          <Route path="list" element={<ListProject />} />
          <Route path="create" element={<CreateProject />} />
          <Route path="detail/:id" element={<ProjectDetail />}></Route>
          <Route path="edit/:id" element={<EditProject />} />
        </Route>
      </Route>
    )
  );
  return <RouterProvider router={router}></RouterProvider>;
};

export default Router;
