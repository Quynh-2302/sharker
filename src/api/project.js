import api from "./utils";

export const getProjectListAPI = () => {
  return api.get("/projects");
};

export const getProjectDetailAPI = (id) => {
  return api.get(`/projects/${id}`, id);
};

export const createProjectAPI = (values) => {
  return api.post("/projects", values);
};
export const editProjectAPI = (id, values) => {
  return api.put(`/projects/${id}`, values);
};

export const deleteProjectAPI = (id) => {
  return api.delete(`/projects/${id}`, id);
};
export const getRegionsAPI = () => {
  return api.get(`/resources/regions`);
};
