import api from "./utils";

export const loginAPI = (values) => {
  return api.post("/auth/login", values);
};
