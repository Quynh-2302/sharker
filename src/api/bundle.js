import api from "./utils";

export const getBundlesAPI = () => {
  return api.get("/bundles");
};
