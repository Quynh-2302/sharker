import api from "./utils";

export const getProjectIdsAPI = () => {
  return api.get(`/gitlab/projects`);
};

export const getProjectBranchesAPI = (id) => {
  return api.get(`/gitlab/projects/${id}/repository/branches`, id);
};

export const getVariablesAPI = (id) => {
  return api.get(`/gitlab/projects/${id}/variables`, id);
};
