import api from "./utils";

export const getConfigsListAPI = () => {
  return api.get("/configs");
};
export const createConfigsAPI = (values) => {
  return api.post(`/configs`, values);
};
export const getConfigsDetailAPI = (id) => {
  return api.get(`/configs/${id}`, id);
};
export const deleteConfigsAPI = (id) => {
  return api.delete(`/configs/${id}`, id);
};
export const editConfigAPI = (id, values) => {
  return api.put(`/configs/${id}`, values);
};
export const getLoadBalanceListAPI = () => {
  return api.get(`/load-balance`);
};

export const getAllInstance = () => {
  return api.get(`/instance`);
};

export const createInstanceAPI = (values) => {
  return api.put(`/strategy/scale-up`, values);
};
export const deleteInstanceAPI = (instanceId) => {
  return api.put(`/strategy/scale-down`, instanceId);
};
export const getInstanceAPI = (id) => {
  return api.get(`/instance?page=1&limit=100&filter={"configId__": "${id}"}`);
};
