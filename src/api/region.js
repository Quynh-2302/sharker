import api from "./utils";

export const getRegionsAPI = () => {
  return api.get(`/resources/regions`);
};

export const getZoneAPI = (region) => {
  return api.get(`resources/regions/${region}/availability-zone`);
};
