import axios from "axios";

const api = axios.create({
  baseURL: `${process.env.REACT_APP_SERVER_URL}/api/v1`,
  headers: {
    "Content-Type": "application/json",
  },
});

api.interceptors.request.use(function (config) {
  const token = localStorage.getItem("token");
  const APIkey = localStorage.getItem("APIkey");

  config.headers = {
    Authorization: `Bearer ${token}`,
    "X-API-Key": `${APIkey}`,
  };
  return config;
});

export default api;
