import { Form, Input, Col, Row, Radio, Select, Upload, Card } from "antd";
import { UploadIcon } from "../../../assets/logo/icon";
import { useDispatch, useSelector } from "react-redux";
import { createProjectAction } from "../../../redux/projects/action";
import PrivateLayout from "../../../layout/PrivateLayout";
import Header from "../../../components/Header";
import { SaveOutlined } from "@ant-design/icons";
import { Layout } from "antd";
import { getRegionsAction } from "../../../redux/regions/actions";
import { useEffect } from "react";
import { getRegion, getRegionSelector } from "../../../redux/config/selector";

const normFile = (e) => {
  if (Array.isArray(e)) {
    return e;
  }
  return e?.fileList;
};
const { Content } = Layout;
const VALIDATE = {
  projectName: [
    {
      required: true,
      message: "Please input project name",
    },
  ],
  accessKey: [
    {
      required: true,
      message: "Please input access key",
    },
  ],
  region: [{ required: true, message: "Please select a region" }],
  sercetKey: [{ required: true, message: "Please input your secret key" }],
};

const CreateProject = () => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const handleCreate = async (values) => {
    dispatch(createProjectAction(values));
    form.resetFields();
  };
  const handleCancel = () => {
    form.resetFields();
  };
  const buttons = [
    {
      label: "Cancle",
      onClick: handleCancel,
      className: "secondButton",
    },
    {
      label: "Save",
      Icon: <SaveOutlined />,
      onClick: form.submit,
      className: "primaryButton",
    },
  ];

  useEffect(() => {
    dispatch(getRegionsAction());
  }, [dispatch]);
  const regions = useSelector(getRegionSelector);

  return (
    <PrivateLayout>
      <Header title="Project List / Create" buttons={buttons} />
      <Content style={{ overflow: "auto" }}>
        <Content style={{ margin: "24px 16px 0" }}>
          <Card
            title="General Information"
            style={{ minHeight: 360, background: "var(--bg-color)" }}
          >
            <Form
              form={form}
              onFinish={handleCreate}
              labelCol={{ span: 12 }}
              wrapperCol={{ span: 9 }}
              style={{ margin: "0px 96px" }}
              layout="vertical"
            >
              <Row gutter={60}>
                <Col span={19} style={{ textAlign: "left" }}>
                  <Form.Item
                    label="Project Name"
                    name="projectName"
                    rules={VALIDATE.projectName}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item label="Git token" name="gitPersonalToken">
                    <Input />
                  </Form.Item>

                  <Form.Item label="Status" name="status">
                    <Radio.Group>
                      <Radio value="true">Active</Radio>
                      <Radio value="flase">Inactive</Radio>
                    </Radio.Group>
                  </Form.Item>

                  <Form.Item
                    label="Access key"
                    name="accessKey"
                    rules={VALIDATE.accessKey}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Secret key"
                    name="secretKey"
                    rules={VALIDATE.sercetKey}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Region"
                    name="region"
                    rules={VALIDATE.region}
                  >
                    <Select>
                      {regions.map((item, index) => (
                        <Select.Option key={index} value={`${item.code}`}>
                          {item.continentCode} ({item.displayName}) {"("}
                          {item.code} {")"}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={5}>
                  <Form.Item
                    name="picture"
                    label="Image"
                    valuePropName="fileList"
                    getValueFromEvent={normFile}
                    style={{ textAlign: "right" }}
                  >
                    <Upload listType="picture-card">
                      <div>
                        <UploadIcon />
                      </div>
                    </Upload>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Card>
        </Content>
      </Content>
    </PrivateLayout>
  );
};
export default CreateProject;
