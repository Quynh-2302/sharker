import React from "react";
import "../../../styles/global.css";
import ProjectListTable from "../../../components/Table/ListTable";
import PrivateLayout from "../../../layout/PrivateLayout";
import Header from "../../../components/Header";
import { PlusOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";

const ListProject = () => {
  const navigate = useNavigate();
  const handleButtonClick = () => {
    navigate("/project/create");
  };
  const buttons = [
    {
      label: "New",
      Icon: <PlusOutlined />,
      onClick: handleButtonClick,
      className: "primaryButton",
    },
  ];
  return (
    <PrivateLayout>
      <Header title="Project List" showButton buttons={buttons} />
      <ProjectListTable />
    </PrivateLayout>
  );
};
export default ListProject;
