import styled from "styled-components";
import { Layout } from "antd";
import "../../../styles/global.css";

export const CustomContent = styled(Layout.Content)`
  margin: 24px 16px 0;
  flex-grow: 2;
`;
