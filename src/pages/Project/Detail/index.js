import React from "react";
import { Layout } from "antd";
import "../../../styles/global.css";
import { CustomContent } from "./styles";
import PrivateLayout from "../../../layout/PrivateLayout";
import Header from "../../../components/Header";
import InforCard from "../../../components/Card/InfoCard";
import ConfigTable from "../../../components/Table/ConfigTable";

const { Content } = Layout;
const ProjectDetail = () => {
  return (
    <PrivateLayout>
      <Header title="Project List" />
      <Content style={{ overflow: "auto" }}>
        <CustomContent>
          <InforCard />
          <ConfigTable />
        </CustomContent>
      </Content>
    </PrivateLayout>
  );
};
export default ProjectDetail;
