import { useEffect } from "react";
import {
  Layout,
  Form,
  Input,
  Col,
  Row,
  Radio,
  Select,
  Upload,
  Card,
} from "antd";
import { UploadIcon } from "../../../assets/logo/icon";
import { useDispatch, useSelector } from "react-redux";
import PrivateLayout from "../../../layout/PrivateLayout";
import Header from "../../../components/Header";
import { SaveOutlined, PlusOutlined } from "@ant-design/icons";
import { SaveButton } from "../../../styles/components";
import { useParams } from "react-router-dom";
import {
  getDetailProjectAction,
  editProjectAction,
} from "../../../redux/projects/action";
import { getRegionsAction } from "../../../redux/regions/actions";
import { getProjectByIdSelector } from "../../../redux/projects/selector";

const normFile = (e) => {
  console.log("Upload event:", e);
  if (typeof e === "string") {
    return e;
  }
  return e?.file?.response?.url;
};

const EditProject = () => {
  const { Content } = Layout;
  const { id } = useParams();
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  //Hanlde edit button
  const handleEdit = async (values) => {
    dispatch(editProjectAction({ id, values }));
  };

  const buttons = [
    {
      label: "Cancle",
      onClick: form.resetFields,
      className: "secondButton",
    },
    {
      label: "Save",
      Icon: <SaveOutlined />,
      onClick: form.submit,
      className: "primaryButton",
    },
  ];

  //Get project's information
  useEffect(() => {
    dispatch(getDetailProjectAction(id));
    dispatch(getRegionsAction());
  }, [dispatch]);

  const dataSource = useSelector((state) =>
    getProjectByIdSelector(state, { id })
  );

  useEffect(() => {
    if (dataSource) {
      form.setFieldsValue({
        projectName: dataSource.projectName,
        gitPersonalToken: dataSource.gitPersonalToken,
        isActive: dataSource.isActive ? true : false,
        region: dataSource.region,
        accessKey: dataSource.accessKey,
        apiKey: dataSource.apiKey,
        image: dataSource.image,
      });
    }
  }, [dataSource, form]);

  //Load regions
  const regions = useSelector((state) => state.regions.data);

  return (
    <>
      {dataSource ? (
        <PrivateLayout>
          <Header
            title={form.getFieldValue("projectName") + " / EDIT"}
            buttons={buttons}
          />
          <Content style={{ overflow: "auto" }}>
            <Card title="General Information" style={{ margin: "24px 16px 0" }}>
              <Form
                form={form}
                onFinish={handleEdit}
                labelCol={{ span: 24 }}
                wrapperCol={{ span: 20 }}
                layout="vertical"
                initialValues=""
              >
                <Row gutter={5}>
                  <Col span={8}>
                    <Form.Item label="Project Name" name="projectName">
                      <Input />
                    </Form.Item>

                    <Form.Item
                      label="Status"
                      name="isActive"
                      initialValue={true}
                    >
                      <Radio.Group>
                        <Radio value={true}>Active</Radio>
                        <Radio value={false}>Inactive</Radio>
                      </Radio.Group>
                    </Form.Item>

                    <Form.Item label="API Key" name="apiKey">
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="Git token" name="gitPersonalToken">
                      <Input />
                    </Form.Item>

                    <Form.Item label="Region" name="region">
                      <Select>
                        {regions.map((item, index) => (
                          <Select.Option key={index} value={item.code}>
                            {item.continentCode} ({item.displayName}) {"("}
                            {item.code} {")"}
                          </Select.Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item label="Access key" name="accessKey">
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      name="picture"
                      label="Image"
                      valuePropName="fileList"
                      getValueFromEvent={normFile}
                    >
                      <Upload listType="picture-card">
                        <div>
                          <img
                            src={dataSource.image}
                            alt="Image"
                            style={{ maxWidth: "70%", maxHeight: "70%" }}
                          />
                          <UploadIcon />
                        </div>
                      </Upload>
                    </Form.Item>

                    <Form.Item label="Secret key" name="secretKey">
                      <Input />
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Card>
            <Card
              title="Local Balance"
              style={{ margin: "16px" }}
              extra={
                <SaveButton>
                  <PlusOutlined /> New
                </SaveButton>
              }
            >
              <Form
                // form={form}
                // onFinish={handleCreate}
                labelCol={{ span: 24 }}
                wrapperCol={{ span: 20 }}
                style={{ margin: "0px 30px" }}
                layout="vertical"
              >
                <Row gutter={5}>
                  <Col span={8} style={{ textAlign: "left" }}>
                    <Form.Item label="ID" name="id">
                      <Input />
                    </Form.Item>

                    <Form.Item label="Status" name="status">
                      <Radio.Group>
                        <Radio value="active">Active</Radio>
                        <Radio value="inactive">Inactive</Radio>
                      </Radio.Group>
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="IP" name="ip">
                      <Input />
                    </Form.Item>
                    <Form.Item label="SSH key" name="SSH">
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="Name" name="name">
                      <Input />
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Card>
          </Content>
        </PrivateLayout>
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
};
export default EditProject;
