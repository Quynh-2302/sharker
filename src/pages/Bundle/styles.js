import { Layout } from "antd";
import styled from "styled-components";
import "../../styles/global.css";

const { Header } = Layout;
export const StyledHeader = styled(Header)`
  padding: 35px 35px 35px 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: var(--bg-color);

  .textBox {
    .test {
      display: flex;
      padding: 12px 10px;
      justify-content: space-between;
      align-items: center;
    }

    .test input {
      margin: 5px 7px;
    }

    .test svg {
      width: 10px;
      height: 3px;
      fill: none;
      path {
        fill: var(--icon-color);
      }
    }
  }
  .backButton {
    display: inline-flex;
    padding: 5px;
    align-items: flex-start;
    gap: 6px;
    border: none;
    border-radius: 5px;
    background: rgba(68, 39, 227, 0.05);
    color: var(--primary-color);
  }
  .addButton {
    background: var(--primary-color);
    float: right;
  }
  .ant-btn-primary:not(:disabled):hover {
    background: var(--bg-color);
    color: var(--primary-color);
    border: 1px solid var(--primary-color);
  }
`;
