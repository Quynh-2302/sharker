import React from "react";
import "../../styles/global.css";
import PrivateLayout from "../../layout/PrivateLayout";
import Header from "../../components/Header";
import BundleTable from "../../components/Table/BundleTable";
import { PlusOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";

const BundlePage = () => {
  const navigate = useNavigate();
  const handleButtonClick = () => {
    navigate("/project/create");
  };
  const buttons = [
    {
      label: "New",
      Icon: <PlusOutlined />,
      onClick: handleButtonClick,
      className: "primaryButton",
    },
  ];
  return (
    <PrivateLayout>
      <Header title="Bundle" buttons={buttons} />
      <BundleTable />
    </PrivateLayout>
  );
};
export default BundlePage;
