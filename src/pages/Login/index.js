import React from "react";
import PublicLayout from "../../layout/PublicLayout";
import LoginForm from "../../containers/Login";

const Login = () => {
  return (
    <PublicLayout>
      <LoginForm />
    </PublicLayout>
  );
};
export default Login;
