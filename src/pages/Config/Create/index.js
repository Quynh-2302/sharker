import { useEffect, useState } from "react";
import { Layout, Form, Col, Row, Radio, Select } from "antd";
import { useDispatch, useSelector } from "react-redux";
import PrivateLayout from "../../../layout/PrivateLayout";
import Header from "../../../components/Header";
import {
  SaveOutlined,
  CaretDownOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { useParams } from "react-router-dom";
import {
  ConfigCard,
  CustomInput,
  CustomSelect,
  CustomInputNumber,
} from "./styles";
import {
  createConfigsAction,
  getLoadBalanceAction,
  getZoneAction,
} from "../../../redux/config/action";
import {
  getProjectBranchesAction,
  getProjectIdsAction,
  getVariablesAction,
} from "../../../redux/config/gitProject/action";
import { getDetailProjectAction } from "../../../redux/projects/action";
import { getProjectByIdSelector } from "../../../redux/projects/selector";
import {
  getAllBalancesSelector,
  getAllZonesSelector,
} from "../../../redux/config/selector";
import {
  getAllBranchesSelector,
  getAllProjectIdsSelector,
  getAllVariablesSelector,
} from "../../../redux/config/gitProject/selector";
import BundleCard from "../../../components/Card/BundleCard";

const CreateConfig = () => {
  const { Content } = Layout;
  const { id } = useParams();
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const handleCreate = async (values) => {
    dispatch(
      createConfigsAction({
        ...values,
        bundleId,
        projectId: id,
        ipAddressType: "ipv4",
      })
    );
  };
  const handleCancel = () => {
    form.resetFields();
  };
  const buttons = [
    {
      label: "Cancle",
      onClick: handleCancel,
      className: "secondButton",
    },
    {
      label: "Save",
      Icon: <SaveOutlined />,
      onClick: form.submit,
      className: "primaryButton",
    },
  ];

  //Get project's information, regions

  useEffect(() => {
    dispatch(getDetailProjectAction(id));
    dispatch(getProjectIdsAction());
    dispatch(getLoadBalanceAction());
  }, [dispatch, id]);

  const handleProjectSelect = (id) => {
    dispatch(getProjectBranchesAction(id));
    dispatch(getVariablesAction(id));
  };

  const dataSource = useSelector((state) =>
    getProjectByIdSelector(state, { id })
  );
  useEffect(() => {
    if (dataSource) {
      const region = dataSource.region;
      dispatch(getZoneAction(region));
    }
  }, [dataSource, dispatch]);

  const zone = useSelector(getAllZonesSelector);
  const projectIds = useSelector(getAllProjectIdsSelector);
  const branches = useSelector(getAllBranchesSelector);
  const variables = useSelector(getAllVariablesSelector);
  const balances = useSelector(getAllBalancesSelector);
  const onChange = (value) => {};

  //Bundle
  const [selectedItem, setSelectedItem] = useState(null);
  const [bundleId, setbundleId] = useState(null);
  const handleRadioChange = (item) => {
    setSelectedItem(item);
    setbundleId(item.id);
  };
  return (
    <>
      {dataSource ? (
        <PrivateLayout>
          <Header
            title={dataSource.projectName + " / New config"}
            buttons={buttons}
          />
          <Content style={{ overflow: "auto" }}>
            <ConfigCard title="General Information">
              <Form
                form={form}
                onFinish={handleCreate}
                labelCol={{ span: 24 }}
                wrapperCol={{ span: 17 }}
                layout="vertical"
                initialValues=""
              >
                <Row>
                  <Col span={8}>
                    <Form.Item label="Name" name="name">
                      <CustomInput />
                    </Form.Item>
                    <Form.Item
                      label="Availability zone"
                      name="availabilityZone"
                    >
                      <CustomSelect suffixIcon={<CaretDownOutlined />}>
                        {zone.map((item, zoneName) => (
                          <Select.Option
                            key={zoneName}
                            value={`${item.zoneName}`}
                          >
                            {item.zoneName}
                          </Select.Option>
                        ))}
                      </CustomSelect>
                    </Form.Item>
                    <Form.Item label="Min instance" name="minInstant">
                      <CustomInputNumber
                        min={1}
                        max={10}
                        defaultValue={1}
                        onChange={onChange}
                      />
                    </Form.Item>

                    <Form.Item label="SSH key" name="sshKey">
                      <CustomInput />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      label="Status"
                      name="isActive"
                      initialValue={true}
                    >
                      <Radio.Group>
                        <Radio value={true}>Active</Radio>
                        <Radio value={false}>Inactive</Radio>
                      </Radio.Group>
                    </Form.Item>
                    <Form.Item label="Private port" name="privatePort">
                      <CustomInput />
                    </Form.Item>
                    <Form.Item label="Local balance" name="loadBalanceId">
                      <CustomSelect
                        suffixIcon={<CaretDownOutlined />}
                        showSearch
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          (option?.label ?? "").includes(input)
                        }
                        filterSort={(optionA, optionB) =>
                          (optionA?.label ?? "")
                            .toLowerCase()
                            .localeCompare((optionB?.label ?? "").toLowerCase())
                        }
                      >
                        {balances.map((item, index) => (
                          <Select.Option key={index} value={item}>
                            {item}
                          </Select.Option>
                        ))}
                      </CustomSelect>
                    </Form.Item>
                  </Col>

                  <Col span={8}>
                    <Form.Item label="Git project" name="gitProjectId">
                      <CustomSelect
                        suffixIcon={<SearchOutlined />}
                        showSearch
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          (option?.label ?? "").includes(input)
                        }
                        filterSort={(optionA, optionB) =>
                          (optionA?.label ?? "")
                            .toLowerCase()
                            .localeCompare((optionB?.label ?? "").toLowerCase())
                        }
                        onSelect={handleProjectSelect}
                      >
                        {projectIds.map((item, id) => (
                          <Select.Option key={id} value={item}>
                            {item}
                          </Select.Option>
                        ))}
                      </CustomSelect>
                    </Form.Item>
                    <Form.Item label="Git branch" name="gitBranchName">
                      <CustomSelect
                        suffixIcon={<SearchOutlined />}
                        showSearch
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          (option?.label ?? "").includes(input)
                        }
                        filterSort={(optionA, optionB) =>
                          (optionA?.label ?? "")
                            .toLowerCase()
                            .localeCompare((optionB?.label ?? "").toLowerCase())
                        }
                      >
                        {branches.map((item, name) => (
                          <Select.Option key={name} value={item}>
                            {item}
                          </Select.Option>
                        ))}
                      </CustomSelect>
                    </Form.Item>

                    <Form.Item label="Git variables" name="gitVariableEnvFile">
                      <CustomSelect
                        suffixIcon={<CaretDownOutlined />}
                        showSearch
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          (option?.label ?? "").includes(input)
                        }
                        filterSort={(optionA, optionB) =>
                          (optionA?.label ?? "")
                            .toLowerCase()
                            .localeCompare((optionB?.label ?? "").toLowerCase())
                        }
                      >
                        {variables.map((item, key) => (
                          <Select.Option key={key} value={item}>
                            {item}
                          </Select.Option>
                        ))}
                      </CustomSelect>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </ConfigCard>
            <BundleCard
              selectedItem={selectedItem}
              handleRadioChange={handleRadioChange}
            />
          </Content>
        </PrivateLayout>
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
};
export default CreateConfig;
