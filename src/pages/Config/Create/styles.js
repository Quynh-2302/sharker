import styled from "styled-components";
import { Card, Input, InputNumber, Select, Row, Table } from "antd";
import "../../../styles/global.css";

export const ConfigCard = styled(Card)`
  margin: 24px 16px;
  min-width: 750px;

  &.editBundle {
    border: none;
    margin: 12px;
    .ant-card-body {
      padding: 0px;
    }
  }
  .ant-card-head-title {
    font-size: 15px;
    font-weight: 650;
  }
  .ant-card-head .ant-tabs-top {
    color: var(--grey-300, #a1b0cc);
  }
  .ant-card-body {
    padding: 10px 14px;
  }
  .ant-card-body > .ant-form {
    padding: 24px 10px 24px 71px;
  }
`;
const { Search } = Input;
export const SearchInput = styled(Search)`
  > .ant-input-group
    > .ant-input-group-addon:last-child
    .ant-input-search-button {
    color: var(--grey-400);
    background: var(--bg-input-color);
    font-weight: 900;
    padding: 0px;
    border-color: var(--border-input-color);
    width: 30px;
    box-shadow: none;
  }
  .ant-input-affix-wrapper {
    border-color: var(--border-input-color);
  }
  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):hover {
    border-color: transparent;
  }
  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):focus {
    box-shadow: none;
    border: none;
  }
`;

export const CustomInput = styled(Input)`
  border-color: var(--border-input-color);
  :hover {
    border: none;
  }
`;
export const CustomInputNumber = styled(InputNumber)`
  width: 100%;
  border-color: var(--border-input-color);
  .ant-input-number-handler-wrap {
    opacity: 1;
    border-radius: 0px 10px 10px 0px;
  }
`;
export const CustomSelect = styled(Select)`
  &.ant-select .ant-select-selector {
    border-color: var(--border-input-color);
  }
  &.ant-select .ant-select-arrow .anticon:not(.ant-select-suffix) {
    font-size: 14px;
    color: var(--grey-400);
  }
`;

export const BundleSelected = styled(Table)`
  &.ant-table-wrapper .ant-table-thead tr th {
    background: none;
  }
  &.ant-table-wrapper
    .ant-table-thead
    > tr
    > th:not(:last-child):not(.ant-table-selection-column):not(
      .ant-table-row-expand-icon-cell
    ):not([colspan])::before {
    background: none;
  }
  &.ant-table-wrapper table tr td.ant-table-selection-column {
    border-left: 1px solid #edeef2;
    border-radius: 10px 0px 0px 10px;
  }
  &.ant-table-wrapper .ant-table-tbody > tr.ant-table-row > td {
    border: 1px 0xp solid var(--primary-color);
  }
`;
export const CustomRow = styled(Row)`
  border-radius: 10px;
  border: 1px solid var(--line, #edeef2);
  background: #fff;
  margin: 10px 0px !important;
  width: 100%;
  height: 53px;

  .ant-col {
    display: flex;
    align-items: center;
    overflow: hidden;
  }
  .selectButton {
    padding-left: 0px !important;
  }
  .radio {
    padding-left: 0px;
  }
  &.selected-row {
    border-radius: 10px;
    border: 2px solid var(--primary, #6852d3);
  }
`;
