import { useEffect, useState } from "react";
import { Form, Col, Row, Radio, Select } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { CaretDownOutlined, SearchOutlined } from "@ant-design/icons";
import { useParams } from "react-router-dom";
import {
  ConfigCard,
  CustomInput,
  CustomSelect,
  CustomInputNumber,
} from "../../Create/styles";
import {
  getConfigDetailAction,
  getLoadBalanceAction,
  getZoneAction,
} from "../../../../redux/config/action";
import {
  getProjectBranchesAction,
  getProjectIdsAction,
  getVariablesAction,
} from "../../../../redux/config/gitProject/action";
import {
  getAllBalances,
  getAllBalancesSelector,
  getAllZones,
  getAllZonesSelector,
  getDetailConfig,
  getDetailConfigSelector,
} from "../../../../redux/config/selector";
import {
  getAllBranches,
  getAllBranchesSelector,
  getAllProjectIds,
  getAllProjectIdsSelector,
  getAllVariables,
  getAllVariablesSelector,
} from "../../../../redux/config/gitProject/selector";

const GeneralConfig = ({ form, handleEdit }) => {
  const { id } = useParams();
  const dispatch = useDispatch();

  //get config's detail
  useEffect(() => {
    dispatch(getConfigDetailAction(id));
  }, [dispatch, id]);

  const detail = useSelector((state) =>
    getDetailConfigSelector(state, {
      id,
    })
  );
  useEffect(() => {
    if (detail) {
      form.setFieldsValue({
        name: detail.name,
        availabilityZone: detail.availabilityZone,
        isActive: detail.isActive ? true : false,
        sshKey: detail.sshKey,
        privatePort: detail.privatePort,
        loadBalanceId: detail.loadBalanceId,
        minInstant: detail.minInstant,
      });
    }
  }, [detail, form]);

  //Get project's information, regions
  const region = localStorage.getItem("zone");
  useEffect(() => {
    dispatch(getProjectIdsAction());
    dispatch(getLoadBalanceAction());
    dispatch(getZoneAction(region));
    dispatch(getLoadBalanceAction());
  }, [dispatch, id]);

  const handleProjectSelect = (id) => {
    dispatch(getProjectBranchesAction(id));
    dispatch(getVariablesAction(id));
  };

  //zone
  const zone = useSelector(getAllZonesSelector);
  const projectIds = useSelector(getAllProjectIdsSelector);
  const branches = useSelector(getAllBranchesSelector);
  const variables = useSelector(getAllVariablesSelector);
  const balances = useSelector(getAllBalancesSelector);
  const onChange = (value) => {
    console.log("changed", value);
  };

  return (
    <>
      {detail ? (
        <ConfigCard title="General Information">
          <Form
            form={form}
            onFinish={handleEdit}
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 17 }}
            layout="vertical"
          >
            <Row>
              <Col span={8}>
                <Form.Item label="Name" name="name">
                  <CustomInput />
                </Form.Item>
                <Form.Item label="Availability zone" name="availabilityZone">
                  <CustomSelect suffixIcon={<CaretDownOutlined />}>
                    {zone.map((item, zoneName) => (
                      <Select.Option key={zoneName} value={`${item.zoneName}`}>
                        {item.zoneName}
                      </Select.Option>
                    ))}
                  </CustomSelect>
                </Form.Item>
                <Form.Item label="Min instance" name="minInstant">
                  <CustomInputNumber
                    min={1}
                    max={10}
                    defaultValue={1}
                    onChange={onChange}
                  />
                </Form.Item>

                <Form.Item label="SSH key" name="sshKey">
                  <CustomInput />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="Status" name="isActive" initialValue={true}>
                  <Radio.Group>
                    <Radio value={true}>Active</Radio>
                    <Radio value={false}>Inactive</Radio>
                  </Radio.Group>
                </Form.Item>
                <Form.Item label="Private port" name="privatePort">
                  <CustomInput />
                </Form.Item>
                <Form.Item label="Local balance" name="loadBalanceId">
                  <CustomSelect
                    suffixIcon={<CaretDownOutlined />}
                    showSearch
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      (option?.label ?? "").includes(input)
                    }
                    filterSort={(optionA, optionB) =>
                      (optionA?.label ?? "")
                        .toLowerCase()
                        .localeCompare((optionB?.label ?? "").toLowerCase())
                    }
                  >
                    {balances.map((item, id) => (
                      <Select.Option key={id} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </CustomSelect>
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item label="Git project" name="gitProjectId">
                  <CustomSelect
                    suffixIcon={<SearchOutlined />}
                    showSearch
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      (option?.label ?? "").includes(input)
                    }
                    filterSort={(optionA, optionB) =>
                      (optionA?.label ?? "")
                        .toLowerCase()
                        .localeCompare((optionB?.label ?? "").toLowerCase())
                    }
                    onSelect={handleProjectSelect}
                  >
                    {projectIds.map((item, id) => (
                      <Select.Option key={id} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </CustomSelect>
                </Form.Item>
                <Form.Item label="Git branch" name="gitBranchName">
                  <CustomSelect
                    suffixIcon={<SearchOutlined />}
                    showSearch
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      (option?.label ?? "").includes(input)
                    }
                    filterSort={(optionA, optionB) =>
                      (optionA?.label ?? "")
                        .toLowerCase()
                        .localeCompare((optionB?.label ?? "").toLowerCase())
                    }
                  >
                    {branches.map((item, name) => (
                      <Select.Option key={name} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </CustomSelect>
                </Form.Item>

                <Form.Item label="Git variables" name="gitVariableEnvFile">
                  <CustomSelect
                    suffixIcon={<CaretDownOutlined />}
                    showSearch
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      (option?.label ?? "").includes(input)
                    }
                    filterSort={(optionA, optionB) =>
                      (optionA?.label ?? "")
                        .toLowerCase()
                        .localeCompare((optionB?.label ?? "").toLowerCase())
                    }
                  >
                    {variables.map((item, id) => (
                      <Select.Option key={id} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </CustomSelect>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </ConfigCard>
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
};
export default GeneralConfig;
