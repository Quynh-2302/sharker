import { useEffect, useState } from "react";
import { Layout, Form } from "antd";
import { SaveOutlined } from "@ant-design/icons";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import PrivateLayout from "../../../layout/PrivateLayout";
import Header from "../../../components/Header";
import BundleConfig from "./Bundle";
import GeneralConfig from "./General";
import Instance from "./Instance";
import { ConfigCard } from "../Create/styles";
import {
  editConfigsAction,
  getConfigDetailAction,
} from "../../../redux/config/action";

const tabList = [
  {
    key: "Instance",
    tab: "Instance",
  },
  {
    key: "Bundle",
    tab: "Bundle",
  },
];

const EditConfig = () => {
  const { Content } = Layout;
  const { id } = useParams();
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  //get config's detail
  const [selectedItem, setSelectedItem] = useState();
  useEffect(() => {
    dispatch(getConfigDetailAction(id));
  }, [dispatch, id]);
  const detail = useSelector((state) => state.configs.configById[id]);
  // const selectedItem = detail.bundleId;
  useEffect(() => {
    if (detail) {
      setSelectedItem(detail.bundleId);
    }
  }, [detail]);

  //Bundle
  const handleRadioChange = (item) => {
    setSelectedItem(item.id);
  };

  const handleEdit = async (values) => {
    const body = {
      ...values,
      bundleId: selectedItem,
      ipAddressType: "ipv4",
    };
    dispatch(editConfigsAction({ id, body }));
  };
  const handleCancel = () => {
    form.resetFields();
  };
  const buttons = [
    {
      label: "Cancle",
      onClick: handleCancel,
      className: "secondButton",
    },
    {
      label: "Save",
      Icon: <SaveOutlined />,
      onClick: form.submit,
      className: "primaryButton",
    },
  ];
  const [activeConfig, setactiveConfig] = useState("Instance");
  const onConfigChange = (key) => {
    setactiveConfig(key);
  };
  const contentList = {
    Instance: <Instance minInstant={detail.minInstant} />,
    Bundle: (
      <BundleConfig
        selectedItem={selectedItem}
        handleRadioChange={handleRadioChange}
      />
    ),
  };

  return (
    <>
      {detail ? (
        <PrivateLayout>
          <Header
            title={form.getFieldValue("name") + " / Config Detail"}
            buttons={buttons}
          />
          <Content style={{ overflow: "auto" }}>
            <GeneralConfig form={form} handleEdit={handleEdit} />

            <ConfigCard
              tabList={tabList}
              activeTabKey={activeConfig}
              onTabChange={onConfigChange}
            >
              {contentList[activeConfig]}
            </ConfigCard>
          </Content>
        </PrivateLayout>
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
};
export default EditConfig;
