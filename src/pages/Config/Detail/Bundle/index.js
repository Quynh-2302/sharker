import { useEffect } from "react";
import { Col, Row, Radio } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { CheckOutlined } from "@ant-design/icons";
import { useParams } from "react-router-dom";
import { CustomRow, ConfigCard } from "../../Create/styles";
import { getBundlesAction } from "../../../../redux/bundles/action";
import { getConfigDetailAction } from "../../../../redux/config/action";
import { TableButton } from "../../../../styles/components";
import {
  getAllBundles,
  getAllBundlesSelector,
} from "../../../../redux/bundles/selector";
import {
  getDetailConfig,
  getDetailConfigSelector,
} from "../../../../redux/config/selector";

const BundleConfig = ({ selectedItem, handleRadioChange }) => {
  const { id } = useParams();
  const dispatch = useDispatch();

  //get config's detail
  useEffect(() => {
    dispatch(getConfigDetailAction(id));
  }, [dispatch, id]);
  const detail = useSelector((state) =>
    getDetailConfigSelector(state, {
      id,
    })
  );

  useEffect(() => {
    dispatch(getBundlesAction());
  }, [dispatch]);
  const bundleData = useSelector(getAllBundlesSelector);

  return (
    <>
      {detail ? (
        <ConfigCard className="editBundle">
          <Row>
            <Col span={1}></Col>
            <Col span={2}></Col>
            <Col span={2}>Pricing</Col>
            <Col span={4}>Supported flatforms</Col>
            <Col span={2}>CPU Count</Col>
            <Col span={3}>Disk (GB)</Col>
            <Col span={2}>Power</Col>
            <Col span={2}>Ram (GB)</Col>
            <Col span={4}>Monthly transfer (GB)</Col>
            <Col span={2}></Col>
          </Row>

          {bundleData.map((item, id) => (
            <CustomRow
              key={item.id}
              className={selectedItem === item.id ? "selected-row" : ""}
            >
              <Col span={1}>
                <Radio
                  checked={item.id === selectedItem}
                  onChange={() => handleRadioChange(item)}
                  style={{ paddingLeft: "10px" }}
                />
              </Col>

              <Col span={2}>{item.name}</Col>
              <Col span={2}>{item.price}</Col>
              <Col span={4}>{item.support}</Col>
              <Col span={2}>{item.cpu}</Col>
              <Col span={3}>{item.code}</Col>
              <Col span={2}>{item.power}</Col>
              <Col span={2}>{item.ram}</Col>
              <Col span={4}>{item.monthlyTransfer}</Col>
              <Col span={2}>
                <div
                  checked={item.id === selectedItem}
                  onChange={() => handleRadioChange(item)}
                >
                  {item.id === selectedItem ? (
                    <TableButton className="primaryButton">
                      <CheckOutlined />
                      Selected
                    </TableButton>
                  ) : (
                    <TableButton className="secondButton">Select</TableButton>
                  )}
                </div>
              </Col>
            </CustomRow>
          ))}
        </ConfigCard>
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
};
export default BundleConfig;
