import { Card } from "antd";
import { styled } from "styled-components";

export const ConfigCard = styled(Card)`
  border: none;
  padding: 0px;
  .ant-card-body {
    padding: 10px 0px;
  }
  .ant-card-body > .ant-form {
    padding: 24px 10px 24px 71px;
  }
`;
