import React, { useEffect, useState } from "react";
import { Pagination, Button, Col, Radio, Input, Form, Row } from "antd";
import {
  ConfigCard,
  Title,
  CustomBadge,
  CustomModal,
  InstanceForm,
  CardPagination,
} from "./styles";
import { useDispatch, useSelector } from "react-redux";
import {
  createInstanceAction,
  deleteInstanceAction,
  getInstanceAction,
} from "../../../../redux/config/action";
import {
  PlusOutlined,
  CloseCircleFilled,
  CaretDownOutlined,
  SaveOutlined,
  ExclamationCircleFilled,
} from "@ant-design/icons";
import { CustomSelect } from "../../Create/styles";
import { useParams } from "react-router-dom";
import {
  getAllInstances,
  getAllInstancesSelector,
} from "../../../../redux/config/selector";

const Instance = ({ minInstant }) => {
  const [form] = Form.useForm();
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const { id } = useParams();
  const dispatch = useDispatch();
  const custom = [
    { key: "1", value: 1 },
    { key: "2", value: 2 },
    { key: "3", value: 3 },
    { key: "4", value: 4 },
    { key: "5", value: 5 },
    { key: "6", value: 6 },
    { key: "7", value: 7 },
    { key: "8", value: 8 },
    { key: "9", value: 9 },
    { key: "10", value: 10 },
  ];

  //Open Modal
  useEffect(() => {
    form.setFieldsValue({
      name: "Nano - CPU 1 - 0.5 GB",
      amount: "1",
    });
  }, [form]);
  const showModal = () => {
    setOpen(true);
  };
  const handleOk = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      setOpen(false);
      setConfirmLoading(false);
    }, 2000);
  };
  const handleCancel = () => {
    console.log("Clicked cancel button");
    setOpen(false);
  };

  //Load instance
  useEffect(() => {
    dispatch(getInstanceAction(id));
  }, [dispatch]);

  const instanceData = useSelector(getAllInstancesSelector);

  //Pagination
  const [currentPage, setCurrentPage] = useState(1);
  const handleChangePage = (page) => {
    setCurrentPage(page);
  };

  const startIndex = (currentPage - 1) * 7;
  const endIndex = startIndex + 7;
  const slicedData = instanceData.slice(startIndex, endIndex);

  // Radio
  const [value, setValue] = useState(1);
  const onChange = (e) => {
    setValue(e.target.value);
  };

  //Create instance

  // const [test, setTest] = useState();
  const handleCreate = async (values) => {
    // if (value == 1) {
    //     setTest(values.name);
    //   } else {
    //     setTest(values.amount);
    //   }
    const amount = values.amount;
    const configId = id;
    dispatch(createInstanceAction({ configId, amount }));
  };
  // Handle delete
  const { confirm } = CustomModal;
  const showConfirm = (instanceId) => {
    confirm({
      title: "Do you want to delete this insance?",
      icon: <ExclamationCircleFilled />,
      content: "This action will delete instance forever ",
      onOk() {
        dispatch(deleteInstanceAction({ instanceId: instanceId }));
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };
  const error = () => {
    CustomModal.error({
      title: "Error",
      content: "Total instances are not less than min instances.",
    });
  };

  const handleDelete = (instanceId) => {
    if (instanceData.length <= minInstant) {
      error();
    } else {
      showConfirm(instanceId);
    }
  };

  return (
    <>
      <div>
        <CustomBadge>
          <ConfigCard className="createInstance">
            <Button
              shape="circle"
              icon={<PlusOutlined className="plusIcon" />}
              onClick={showModal}
              className="createBtn"
            />

            <CustomModal
              open={open}
              onOk={handleOk}
              confirmLoading={confirmLoading}
              onCancel={handleCancel}
              footer={[
                <Button key="back" onClick={handleCancel}>
                  Cancle
                </Button>,
                <Button
                  key="submit"
                  type="primary"
                  onClick={form.submit}
                  icon={<SaveOutlined />}
                >
                  Save
                </Button>,
              ]}
            >
              <ConfigCard title="New Instance" className="createCard">
                <Radio.Group
                  onChange={onChange}
                  value={value}
                  style={{
                    width: "100%",
                  }}
                >
                  <InstanceForm
                    form={form}
                    onFinish={handleCreate}
                    layout="horizontal"
                  >
                    <Row className="instanceForm">
                      <Radio value={1} className="title">
                        <h4>Default</h4>
                      </Radio>
                      <Form.Item name="name" className="input">
                        <Input
                          placeholder="Nano - CPU 1 - 0.5 GB"
                          disabled
                          defaultValue="Nano - CPU 1 - 0.5 GB"
                          value={value}
                        />
                      </Form.Item>
                    </Row>
                    <Row className="instanceForm">
                      <Radio value={2} className="title">
                        <h4>Custom</h4>
                      </Radio>
                      <Form.Item name="amount" className="input">
                        <CustomSelect suffixIcon={<CaretDownOutlined />}>
                          {custom.map(({ key, value }) => (
                            <CustomSelect key={key} value={value}>
                              {value}
                            </CustomSelect>
                          ))}
                        </CustomSelect>
                      </Form.Item>
                    </Row>
                  </InstanceForm>
                </Radio.Group>
              </ConfigCard>
            </CustomModal>
          </ConfigCard>
        </CustomBadge>

        {slicedData.map((item, id) => (
          <CustomBadge
            count={
              <CloseCircleFilled
                className="icon"
                onClick={() => handleDelete(item.id)}
              />
            }
          >
            <ConfigCard key={id}>
              <div style={{ display: "flex" }}>
                <Title span={9}>
                  <p>Public IP</p>
                  <p>Private IP</p>
                  <p>CPU</p>
                  <p>Type</p>
                  <p>RAM</p>
                </Title>
                <Col span={15} className="content">
                  <p>{item.availabilityZone}</p>
                  <p>{item.name}</p>
                  <p>{item.availabilityZone}</p>
                  <p>{item.region}</p>
                  <p>{item.privateIpAddress}</p>
                </Col>
              </div>
            </ConfigCard>
          </CustomBadge>
        ))}
      </div>

      <CardPagination
        showSizeChanger
        onChange={handleChangePage}
        defaultCurrent={1}
        pageSize={7}
        total={instanceData.length}
        size="small"
      />
    </>
  );
};

export default Instance;
