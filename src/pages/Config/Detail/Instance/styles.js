import { Badge, Card, Col, Form, Modal, Pagination } from "antd";
import { styled } from "styled-components";

export const ConfigCard = styled(Card)`
  border-radius: 10px;
  border: 2px solid #edeef2;
  background: #fff;

  &.createCard {
    border: 0px;
  }
  .ant-card-body {
    box-shadow: none;
    padding: 12px 18px 0px 18px;
    .content p {
      font-weight: 300;
      text-align: right;
      margin-bottom: 6px;
      color: #57585c;
    }
  }
  .plusIcon {
    color: var(--primary-color);
    font-size: 32px !important;
    font-weight: 900;
  }
  .createBtn {
    width: 80px;
    height: 80px;
    border: 3px dashed var(--primary-color);
  }
  &.createInstance {
    justify-content: center;
    align-items: center;
    display: flex;
    height: 140px;
    padding: 68px 80px 80px 80px;
    .ant-card-body {
      padding: 0px;
    }
  }
`;
export const Title = styled(Col)`
  p {
    font-weight: 600;
    margin-bottom: 6px;
    font-size: 14px;
    color: #57585c;
  }
`;

export const CustomBadge = styled(Badge)`
  margin: 22px 10px 10px 10px;
  width: 23%;
  .icon {
    color: var(--primary-color);
  }
`;
export const CustomModal = styled(Modal)`
  .ant-modal-content {
    padding: 0px 0px 10px 0px;
    .ant-modal-footer {
      margin: 0px 25px 12px 0px;
    }
  }
`;

export const InstanceForm = styled(Form)`
  .ant-form-item {
    margin: 14px 0px 16px 0px;
  }
  .ant-form-item .ant-form-item-control-input-content {
    flex: auto;
    max-width: 100%;
    justify-content: space-around;
    display: flex;
    align-items: center;
  }
  .instanceForm {
    justify-content: space-around;
    align-items: center;
    width: 100%;
    .title {
      width: 20%;
      h4 {
        color: var(--primary-color);
        font-weight: 600;
      }
    }
    .input {
      width: 75%;
    }
  }
`;

export const CardPagination = styled(Pagination)`
  float: right;
  margin: 25px 20px 10px 0px;
`;
