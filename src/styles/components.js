import styled from "styled-components";
import { Table, Layout, Button } from "antd";
import "./global.css";

/*-------------------------Table-------------------------------*/
export const CustomContent = styled(Layout.Content)`
  margin: 24px 16px 0;
  flex-grow: 2;
`;
export const TableContainer = styled(Table)`
  font-size: 13px;
  gap: 10px;
  margin-bottom: 16px;
  min-width: 400px;
  thead tr td {
    background: var(--bg-color);
  }
  & tr th {
    background-color: var(--bg-color);
    color: var(--act-color);
    font-weight: bold;
  }

  & td a {
    color: var(--primary-color);
    font-weight: 1200;
    font-size: 16px;
  }
  .active {
    color: var(--act-color);
    background: var(--bg-act-color);
    border-radius: 3px;
    padding: 2px 5px;
    font-size: 14px;
  }
  .deleteButton {
    padding: 0px;
    border: none;
    background-color: transparent;
    color: var(--primary-color);
    box-shadow: none;
    font-size: 16px;
    :hover {
      border: none;
    }
  }
`;
/*-------------------------Button-------------------------------*/
export const SaveButton = styled(Button)`
  background: var(--primary-color);
  color: var(--bg-color);
  border: none;
  border-radius: 7px;
  font-size: 14px;
  height: 32px;
  padding: 4px 15px;
`;

export const TableButton = styled(Button)`
  height: 23px;
  padding: 5px;
  font-size: 13px;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  border: none;
  border-radius: 5px;

  &.ant-btn > .anticon + span {
    margin-inline-start: 5px;
  }
  &.secondButton {
    background: var(--bg-input-color);
    color: var(--primary-color);
  }
  &.primaryButton {
    background: var(--primary-color);
    color: var(--bg-color);

    &.ant-btn-default:not(:disabled):not(.ant-btn-disabled):hover {
      color: var(--bg-color);
    }
  }
`;
