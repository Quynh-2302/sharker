import "./global.css";
export const theme = {
  token: {
    colorPrimary: "#6852d3",
  },
  components: {
    Button: {
      margin: "0px",
    },
    Input: {
      borderRadius: "10px",
      colorBgContainer: "var(--bg-input-color)",
    },
    InputNumber: {
      borderRadius: "10px",
      colorBgContainer: "var(--bg-input-color)",
    },
    Select: {
      borderRadius: "10px",
      colorBgContainer: "var(--bg-input-color)",
    },
  },
};
