import { createAsyncThunk } from "@reduxjs/toolkit";
import { loginAPI } from "../../api/user";
import { notification } from "antd";

export const loginUser = createAsyncThunk(
  "auth/login",
  async (values, { rejectWithValue }) => {
    try {
      const response = await loginAPI(values);
      console.log("Dang nhap thanh cong: ", response);
      if (response) {
        localStorage.setItem("token", response.data.accessToken);
        return response.data;
      }
      return rejectWithValue(response);
    } catch (error) {
      if (error.response && error.response.data) {
        notification.error({
          message: "This account is not available",
          description: "Check your email and password again",
        });
        return rejectWithValue(error.response.data);
      } else {
        return rejectWithValue("An error occurred.");
      }
    }
  }
);
