import { createAsyncThunk } from "@reduxjs/toolkit";
import { notification } from "antd";
import {
  getProjectDetailAPI,
  createProjectAPI,
  editProjectAPI,
  getProjectListAPI,
  deleteProjectAPI,
} from "../../api/project";

export const createProjectAction = createAsyncThunk(
  "project/create",
  async (values, { rejectWithValue }) => {
    try {
      const response = await createProjectAPI(values);
      if (response) {
        const data = response.data;
        notification.success({
          message: "Create successfully",
        });
        return data;
      }
      return rejectWithValue(response);
    } catch (error) {
      throw new Error("Failed to fetch projects create");
    }
  }
);

export const getDetailProjectAction = createAsyncThunk(
  "project/details",
  async (id) => {
    try {
      const response = await getProjectDetailAPI(id);
      const data = response.data;
      if (data) {
        localStorage.setItem("APIkey", response.data.apiKey);
        localStorage.setItem("zone", response.data.region);
        return data;
      }
    } catch (error) {
      throw new Error("Failed to fetch projects detail");
    }
  }
);

export const editProjectAction = createAsyncThunk(
  "project/edit",
  async ({ id, values }) => {
    try {
      const response = await editProjectAPI(id, values);
      if (response) {
        const data = response.data;
        notification.success({
          message: "Edit successfully",
        });

        return data;
      } else {
        notification.error({
          message: "Edit failed",
        });
      }
    } catch (error) {
      notification.error({
        message: "Error occurred while editing",
      });
      throw error;
    }
  }
);

export const getProjectListAction = createAsyncThunk(
  "project/list",
  async () => {
    try {
      const response = await getProjectListAPI();
      const data = response.data.results || [];
      return data;
    } catch (error) {
      throw new Error("Failed to fetch projects list");
    }
  }
);

export const deleteProjectAction = createAsyncThunk(
  "project/delete",
  async (id) => {
    try {
      const response = await deleteProjectAPI(id);
      const data = response;
      notification.success({
        message: "Delete project successfully",
      });
      return {
        ...data,
        id,
      };
    } catch (error) {
      throw new Error("Failed to delete project");
    }
  }
);
