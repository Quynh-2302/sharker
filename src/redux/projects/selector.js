import { createDraftSafeSelector } from "@reduxjs/toolkit";

const projectById = (state) => state.projects.projectId;
const projectAllIds = (state) => state.projects.allIds;

export const getAllProjectsSelector = createDraftSafeSelector(
  [projectById, projectAllIds],
  (data = {}, ids = []) => {
    return ids.map((id) => data[id]);
  }
);

export const getProjectByIdSelector = createDraftSafeSelector(
  [projectById, (state, props) => props],
  (data, { id }) => data[id]
);
