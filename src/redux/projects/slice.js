import { createSlice } from "@reduxjs/toolkit";
import {
  getProjectListAction,
  editProjectAction,
  getDetailProjectAction,
  createProjectAction,
  deleteProjectAction,
} from "./action";

const { actions, reducer } = createSlice({
  name: "projects",
  initialState: {
    projectId: {},
    allIds: [],
    configId: [],
    isEdited: false,
    isCreated: false,
    isDeleted: false,
    loading: false,
    error: null,
  },
  reducers: {
    editSuccess(state, action) {
      state.isEdited = true;
      const editedProject = action.payload;
      state.projectId[editedProject.id] = editedProject;
    },
    createSuccess(state) {
      state.isCreated = true;
    },
  },
  extraReducers: (builder) => {
    builder
      // get list
      .addCase(getProjectListAction.pending, (state) => {
        state.loading = true;
      })
      .addCase(getProjectListAction.fulfilled, (state, action) => {
        state.loading = false;
        const projectList = action.payload;
        const responseData = projectList.reduce(
          (obj, key) => ({
            ...obj,
            [key.id]: key,
          }),
          {}
        );
        state.projectId = responseData;
        state.allIds = Object.keys(responseData);
      })
      .addCase(getProjectListAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      //edit
      .addCase(editProjectAction.pending, (state) => {
        state.isEdited = false;
        state.error = null;
      })
      .addCase(editProjectAction.fulfilled, (state, action) => {
        state.isEdited = true;
        state.error = null;
        const editedProject = action.payload;
        state.projectId[editedProject.id] = editedProject;
      })
      .addCase(editProjectAction.rejected, (state, action) => {
        state.isEdited = false;
        state.error = action.payload;
      })
      //getdetail
      .addCase(getDetailProjectAction.pending, (state) => {
        state.loading = true;
      })
      .addCase(getDetailProjectAction.fulfilled, (state, action) => {
        state.loading = false;
        const detailProject = action.payload;
        state.projectId[detailProject.id] = detailProject;
        state.configId = detailProject.configs.map((config) => config.id);
      })
      .addCase(getDetailProjectAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })

      // create
      .addCase(createProjectAction.pending, (state) => {
        state.isCreated = false;
        state.error = null;
      })
      .addCase(createProjectAction.fulfilled, (state, action) => {
        state.isCreated = true;
        state.error = null;
      })
      .addCase(createProjectAction.rejected, (state, action) => {
        state.isCreated = false;
        state.error = action.payload;
      })
      // delete
      .addCase(deleteProjectAction.pending, (state) => {
        state.isDeleted = false;
        state.error = null;
      })
      .addCase(deleteProjectAction.fulfilled, (state, action) => {
        state.isDeleted = true;
        state.error = null;
        state.allIds = state.allIds.filter((e) => e !== action.payload.id);
      })
      .addCase(deleteProjectAction.rejected, (state, action) => {
        state.isCreated = false;
        state.error = action.payload;
      });
  },
});

export const { editSuccess, createSuccess, detailProject } = actions;

export default reducer;
