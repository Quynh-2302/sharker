import { createAsyncThunk } from "@reduxjs/toolkit";
import { getRegionsAPI } from "../../api/project";

export const getRegionsAction = createAsyncThunk(
  "regions/getRegions",
  async () => {
    const response = await getRegionsAPI();
    if (response) {
      const data = response.data.results;
      return data;
    }
  }
);
