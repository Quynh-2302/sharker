import { createSlice } from "@reduxjs/toolkit";
import { getRegionsAction } from "./actions";

const getRegionsSlice = createSlice({
  name: "regions",
  initialState: {
    data: [],
    loading: false,
    error: null,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getRegionsAction.pending, (state) => {
        state.loading = true;
      })
      .addCase(getRegionsAction.fulfilled, (state, action) => {
        state.loading = false;
        state.data = action.payload;
      })
      .addCase(getRegionsAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export default getRegionsSlice.reducer;
