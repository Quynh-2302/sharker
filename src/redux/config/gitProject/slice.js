import { createSlice } from "@reduxjs/toolkit";
import {
  getProjectBranchesAction,
  getProjectIdsAction,
  getVariablesAction,
} from "./action";

const { actions, reducer } = createSlice({
  name: "gitProjects",
  initialState: {
    byId: {},
    allIds: [],
    allBranches: [],
    allVariables: [],

    isCreated: false,
    loading: false,
    error: null,
  },
  reducers: {
    createSuccess(state) {
      state.isCreated = true;
    },
  },
  extraReducers: (builder) => {
    builder
      //get projectID
      .addCase(getProjectIdsAction.pending, (state) => {
        state.loading = true;
      })
      .addCase(getProjectIdsAction.fulfilled, (state, action) => {
        state.loading = false;
        const gitProjectList = action.payload;
        const responseData = gitProjectList.reduce(
          (obj, key) => ({
            ...obj,
            [key.id]: key,
          }),
          {}
        );
        state.byId = responseData;
        state.allIds = Object.keys(responseData);
      })
      .addCase(getProjectIdsAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })

      //get project branch
      .addCase(getProjectBranchesAction.pending, (state) => {
        state.loading = true;
      })
      .addCase(getProjectBranchesAction.fulfilled, (state, action) => {
        state.loading = false;
        const gitProjectBranch = action.payload;
        const responseData = gitProjectBranch.reduce(
          (obj, key) => ({
            ...obj,
            [key.name]: key,
          }),
          {}
        );
        state.allBranches = Object.keys(responseData);
      })
      .addCase(getProjectBranchesAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })

      //get variables
      .addCase(getVariablesAction.pending, (state) => {
        state.loading = true;
      })
      .addCase(getVariablesAction.fulfilled, (state, action) => {
        state.loading = false;
        const variables = action.payload;
        const responseData = variables.reduce(
          (obj, id) => ({
            ...obj,
            [id.key]: id,
          }),
          {}
        );
        state.allVariables = Object.keys(responseData);
      })
      .addCase(getVariablesAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export const { createSuccess } = actions;

export default reducer;
