import { createDraftSafeSelector } from "@reduxjs/toolkit";

const projectIds = (state) => state.gitProjects.allIds;
const branches = (state) => state.gitProjects.allBranches;
const variables = (state) => state.gitProjects.allVariables;
export const getAllProjectIdsSelector = createDraftSafeSelector(
  [projectIds],
  (projectIds = []) => {
    return projectIds;
  }
);
export const getAllBranchesSelector = createDraftSafeSelector(
  [branches],
  (branches = []) => {
    return branches;
  }
);
export const getAllVariablesSelector = createDraftSafeSelector(
  [variables],
  (variables = []) => {
    return variables;
  }
);
