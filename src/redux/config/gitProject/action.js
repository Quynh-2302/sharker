import { createAsyncThunk } from "@reduxjs/toolkit";
import {
  getProjectBranchesAPI,
  getProjectIdsAPI,
  getVariablesAPI,
} from "../../../api/gitlab";

export const getProjectIdsAction = createAsyncThunk(
  "config/gitlab/project",
  async () => {
    try {
      const response = await getProjectIdsAPI();
      const data = response.data.results;
      if (data) {
        return data;
      }
      throw new Error("Failed to fetch project's gitID'");
    } catch (error) {
      throw new Error("Failed to fetch project's gitID");
    }
  }
);
export const getProjectBranchesAction = createAsyncThunk(
  "config/gitlab/project/branch",
  async (id) => {
    try {
      const response = await getProjectBranchesAPI(id);
      const data = response.data.results;
      if (data) {
        return data;
      }
      throw new Error("Failed to fetch project's branch'");
    } catch (error) {
      throw new Error("Failed to fetch project's branch");
    }
  }
);
export const getVariablesAction = createAsyncThunk(
  "config/gitlab/variables",
  async (id) => {
    try {
      const response = await getVariablesAPI(id);
      const data = response.data.results;
      if (data) {
        return data;
      }
      throw new Error("Failed to fetch project's branch'");
    } catch (error) {
      throw new Error("Failed to fetch project's branch");
    }
  }
);
