import { createDraftSafeSelector } from "@reduxjs/toolkit";

const configById = (state) => state.configs.configById;
const configAllIds = (state) => state.configs.allConfigIds;
const instanceById = (state) => state.configs.instanceById;
const instanceAllIds = (state) => state.configs.instanceAllIds;
const zone = (state) => state.configs.zoneAllIds;
const balances = (state) => state.configs.allBalances;
const regions = (state) => state.regions.data;

export const getAllInstancesSelector = createDraftSafeSelector(
  [instanceById, instanceAllIds],
  (data = {}, ids = []) => {
    return ids.map((id) => data[id]);
  }
);
export const getAllConfigsSelector = createDraftSafeSelector(
  [configById, configAllIds, (state, projectId) => projectId],
  (data = {}, ids = [], projectId) => {
    return ids
      .filter((id) => data[id].projectId === projectId)
      .map((id) => data[id]);
  }
);
export const getDetailConfigSelector = createDraftSafeSelector(
  [configById, (state, props) => props],
  (data, { id }) => data[id]
);
export const getAllZonesSelector = createDraftSafeSelector(
  [zone],
  (zone = {}) => {
    return Object.values(zone);
  }
);
export const getAllBalancesSelector = createDraftSafeSelector(
  [balances],
  (balances = {}) => {
    return Object.values(balances);
  }
);
export const getRegionSelector = createDraftSafeSelector(
  [regions],
  (regions = {}) => {
    return Object.values(regions);
  }
);
