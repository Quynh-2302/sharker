import { createAsyncThunk } from "@reduxjs/toolkit";
import {
  createConfigsAPI,
  createInstanceAPI,
  deleteConfigsAPI,
  deleteInstanceAPI,
  editConfigAPI,
  getConfigsDetailAPI,
  getConfigsListAPI,
  getInstanceAPI,
  getLoadBalanceListAPI,
} from "../../api/config";
import { notification } from "antd";
import { getZoneAPI } from "../../api/region";

//config action
export const getConfigAction = createAsyncThunk(
  "config/allConfig",
  async (id) => {
    try {
      const response = await getConfigsListAPI();
      const data = response.data.results;
      if (data) {
        return data;
      }
      throw new Error("Failed to load instance list");
    } catch (error) {
      throw new Error("Failed to load instance list");
    }
  }
);
export const createConfigsAction = createAsyncThunk(
  "configs/create",
  async (values, { rejectWithValue }) => {
    try {
      const response = await createConfigsAPI(values);
      if (response) {
        const data = response.data;
        notification.success({
          message: "Create successfully",
        });
        return data;
      }
      return rejectWithValue(response);
    } catch (error) {
      throw new Error("Failed to fetch Configs create");
    }
  }
);
export const editConfigsAction = createAsyncThunk(
  "configs/edit",
  async ({ id, body }) => {
    try {
      const response = await editConfigAPI(id, body);
      if (response) {
        const data = response.data;
        notification.success({
          message: "Edit successfully",
        });
        return data;
      }
    } catch (error) {
      throw new Error("Failed to fetch Configs create");
    }
  }
);
export const getConfigDetailAction = createAsyncThunk(
  "configs/details",
  async (id) => {
    try {
      const response = await getConfigsDetailAPI(id);
      const data = response.data;
      if (data) {
        return data;
      }
    } catch (error) {
      throw new Error("Failed to fetch configs detail");
    }
  }
);
export const deleteConfigsAction = createAsyncThunk(
  "configs/delete",
  async (id) => {
    try {
      const response = await deleteConfigsAPI(id);
      const data = response;
      notification.success({
        message: "Delete config successfully",
      });
      return {
        ...data,
        id,
      };
    } catch (error) {
      throw new Error("Failed to delete config");
    }
  }
);
// create action
export const getLoadBalanceAction = createAsyncThunk(
  "config/load-balance",
  async () => {
    try {
      const response = await getLoadBalanceListAPI();
      const data = response.data.results;
      if (data) {
        return data;
      }
      throw new Error("Failed to load balance list");
    } catch (error) {
      throw new Error("Failed to load balance list");
    }
  }
);
export const getZoneAction = createAsyncThunk("config/zone", async (region) => {
  try {
    const response = await getZoneAPI(region);
    const data = response.data.results;
    if (data) {
      return data;
    }
    throw new Error("Failed to fetch projects detail");
  } catch (error) {
    throw new Error("Failed to fetch projects detail");
  }
});
//Instance action
export const getInstanceAction = createAsyncThunk(
  "config/allInstance",
  async (id) => {
    try {
      const response = await getInstanceAPI(id);
      const data = response.data.results;
      if (data) {
        return data;
      }
      throw new Error("Failed to load instance list");
    } catch (error) {
      throw new Error("Failed to load instance list");
    }
  }
);
export const createInstanceAction = createAsyncThunk(
  "instance/create",
  async (instance, { rejectWithValue }) => {
    try {
      const response = await createInstanceAPI(instance);
      if (response) {
        const data = response.data;
        console.log(data);
        notification.success({
          message: "Create successfully",
        });
        return data;
      }
      return rejectWithValue(response);
    } catch (error) {
      throw new Error("Failed to fetch Configs create");
    }
  }
);
export const deleteInstanceAction = createAsyncThunk(
  "instance/delete",
  async (instanceId, { rejectWithValue }) => {
    try {
      const response = await deleteInstanceAPI(instanceId);
      if (response) {
        const data = response;
        console.log(data);
        notification.success({
          message: "Delete successfully",
        });
        return {
          ...data,
          instanceId,
        };
      }
      return rejectWithValue(response);
    } catch (error) {
      throw new Error("Failed to fetch Configs create");
    }
  }
);
