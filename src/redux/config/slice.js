import { createSlice } from "@reduxjs/toolkit";
import {
  createConfigsAction,
  createInstanceAction,
  deleteConfigsAction,
  deleteInstanceAction,
  editConfigsAction,
  getConfigAction,
  getConfigDetailAction,
  getInstanceAction,
  getLoadBalanceAction,
  getZoneAction,
} from "./action";

const { actions, reducer } = createSlice({
  name: "configs",
  initialState: {
    configById: {},
    allConfigIds: [],
    instanceById: {},
    instanceAllIds: [],
    LoadBalances: [],
    allBalances: [],
    zoneAllIds: [],
    isEdited: false,
    isCreated: false,
    loading: false,
    error: null,
  },
  reducers: {
    editSuccess(state, action) {
      state.isEdited = true;
      const editedConfig = action.payload;
      state.configById[editedConfig.id] = editedConfig;
    },
    createSuccess(state) {
      state.isCreated = true;
    },
  },
  extraReducers: (builder) => {
    builder
      //get all configs inf
      .addCase(getConfigAction.pending, (state) => {
        state.loading = true;
      })
      .addCase(getConfigAction.fulfilled, (state, action) => {
        state.loading = false;
        const config = action.payload;
        const responseData = config.reduce(
          (obj, key) => ({
            ...obj,
            [key.id]: key,
          }),
          {}
        );
        state.configById = responseData;
        state.allConfigIds = Object.keys(responseData);
      })
      .addCase(getConfigAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      //config detail
      .addCase(getConfigDetailAction.pending, (state) => {
        state.loading = true;
      })
      .addCase(getConfigDetailAction.fulfilled, (state, action) => {
        state.loading = false;
        const detailConfig = action.payload;
        state.configById = {
          ...state.configById,
          [detailConfig.id]: detailConfig,
        };
        // state.instanceById = detailConfig.configs.map(
        //   (instance) => instance.id
        // );
      })
      .addCase(getConfigDetailAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      //create a config
      .addCase(createConfigsAction.pending, (state) => {
        state.isCreated = false;
        state.error = null;
      })
      .addCase(createConfigsAction.fulfilled, (state, action) => {
        state.isCreated = true;
        state.error = null;
      })
      .addCase(createConfigsAction.rejected, (state, action) => {
        state.isCreated = false;
        state.error = action.payload;
      })
      //edit config
      .addCase(editConfigsAction.pending, (state) => {
        state.isEdited = false;
        state.error = null;
      })
      .addCase(editConfigsAction.fulfilled, (state, action) => {
        state.isEdited = true;
        state.error = null;
        const editedConfig = action.payload;
        state.configById[editedConfig.id] = editedConfig;
      })
      .addCase(editConfigsAction.rejected, (state, action) => {
        state.isEdited = false;
        state.error = action.payload;
      })
      //delete config
      .addCase(deleteConfigsAction.pending, (state) => {
        state.isDeleted = false;
        state.error = null;
      })
      .addCase(deleteConfigsAction.fulfilled, (state, action) => {
        state.isDeleted = true;
        state.error = null;
        state.allConfigIds = state.allConfigIds.filter(
          (e) => e !== action.payload.id
        );
      })
      .addCase(deleteConfigsAction.rejected, (state, action) => {
        state.isCreated = false;
        state.error = action.payload;
      })
      //get load instance list
      .addCase(getInstanceAction.pending, (state) => {
        state.loading = true;
      })
      .addCase(getInstanceAction.fulfilled, (state, action) => {
        state.loading = false;
        const instance = action.payload;
        const responseData = instance.reduce(
          (obj, key) => ({
            ...obj,
            [key.id]: key,
          }),
          {}
        );
        state.instanceById = responseData;
        state.instanceAllIds = Object.keys(responseData);
      })
      .addCase(getInstanceAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      //create instance
      .addCase(createInstanceAction.pending, (state) => {
        state.isCreated = false;
        state.error = null;
      })
      .addCase(createInstanceAction.fulfilled, (state, action) => {
        state.isCreated = true;
        state.error = null;
        const detailInstance = action.payload;
        state.instanceById = {
          ...state.instanceById,
          [detailInstance.id]: detailInstance,
        };
      })
      .addCase(createInstanceAction.rejected, (state, action) => {
        state.isCreated = false;
        state.error = action.payload;
      })
      // delete instance
      .addCase(deleteInstanceAction.pending, (state) => {
        state.isDeleted = false;
        state.error = null;
      })
      .addCase(deleteInstanceAction.fulfilled, (state, action) => {
        state.isDeleted = true;
        state.error = null;
        state.instanceAllIds = state.instanceAllIds.filter(
          (e) => e !== action.payload.instanceId.instanceId
        );
      })
      .addCase(deleteInstanceAction.rejected, (state, action) => {
        state.isCreated = false;
        state.error = action.payload;
      })

      //get availability-zone
      .addCase(getZoneAction.pending, (state) => {
        state.loading = true;
      })
      .addCase(getZoneAction.fulfilled, (state, action) => {
        state.loading = false;
        state.zoneAllIds = action.payload;
      })
      .addCase(getZoneAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      //get load balance list
      .addCase(getLoadBalanceAction.pending, (state) => {
        state.loading = true;
      })
      .addCase(getLoadBalanceAction.fulfilled, (state, action) => {
        state.loading = false;
        const balances = action.payload;
        const responseData = balances.reduce(
          (obj, key) => ({
            ...obj,
            [key.id]: key,
          }),
          {}
        );
        state.LoadBalances = responseData;
        state.allBalances = Object.keys(responseData);
      })
      .addCase(getLoadBalanceAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export const { editSuccess, createSuccess, detailProject } = actions;

export default reducer;
