import { createDraftSafeSelector } from "@reduxjs/toolkit";

const bundleAllIds = (state) => state.bundles.data;

export const getAllBundlesSelector = createDraftSafeSelector(
  [bundleAllIds],
  (bundle = {}) => {
    return Object.values(bundle);
  }
);
