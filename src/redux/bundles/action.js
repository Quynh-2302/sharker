import { createAsyncThunk } from "@reduxjs/toolkit";
import { getBundlesAPI } from "../../api/bundle";

export const getBundlesAction = createAsyncThunk(
  "bundles/getBundlesAPI",
  async () => {
    try {
      const response = await getBundlesAPI();
      const data = response.data.results;
      return data;
    } catch (error) {
      throw new Error("Failed to fetch bundles");
    }
  }
);
