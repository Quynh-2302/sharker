import { createSlice } from "@reduxjs/toolkit";
import { getBundlesAction } from "./action";

const bundlesSlice = createSlice({
  name: "bundles",
  initialState: {
    data: [],
    loading: false,
    error: null,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getBundlesAction.pending, (state) => {
        state.loading = true;
      })
      .addCase(getBundlesAction.fulfilled, (state, action) => {
        state.loading = false;
        state.data = action.payload;
      })
      .addCase(getBundlesAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export default bundlesSlice.reducer;
