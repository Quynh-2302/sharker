import { configureStore, combineReducers } from "@reduxjs/toolkit";
import authReducer from "./auth/slice";
import bundlesReducer from "./bundles/slice";
import getRegionsReducer from "./regions/slice";
import projects from "./projects/slice";
import configs from "./config/slice";
import gitProjects from "./config/gitProject/slice";
import api from "../api/utils";

const rootReducer = combineReducers({
  auth: authReducer,
  bundles: bundlesReducer,
  regions: getRegionsReducer,
  projects,
  configs,
  gitProjects,
});

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      thunk: {
        extraArgument: api,
      },
      serializableCheck: false,
    }),
});

export default store;
