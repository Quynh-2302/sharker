import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, Spin, Input } from "antd";
import "../../styles/global.css";
import { loginUser } from "../../redux/auth/action";
import { antIcon } from "../../assets/logo/icon";
import { useNavigate } from "react-router";

const VALIDATE = {
  username: [
    {
      required: true,
      message: "Please input your username!",
    },
  ],
  password: [
    {
      required: true,
      message: "Please input your password!",
    },
  ],
};

const LoginForm = () => {
  //status
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleLogin = async (values) => {
    dispatch(loginUser(values));
  };

  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);

  useEffect(() => {
    if (isLoggedIn) {
      navigate("/bundle");
    }
  }, [isLoggedIn, navigate]);

  return (
    <div>
      <Form form={form} onFinish={handleLogin}>
        <Form.Item name="email" rules={VALIDATE.username}>
          <Input type="email" placeholder="Username" className="input" />
        </Form.Item>

        <Form.Item name="password" rules={VALIDATE.password}>
          <Input.Password
            type="password"
            placeholder="Password"
            className="input"
          />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            span: 24,
          }}
        >
          <Button className="btnSignIn" onClick={form.submit}>
            {isLoggedIn ? <Spin indicator={antIcon} /> : "Sign in"}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
export default LoginForm;
