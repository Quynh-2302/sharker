import { ConfigProvider, Layout } from "antd";
import React from "react";
import "../../styles/global.css";
import { theme } from "../../styles/layout";
import Sidebar from "../../components/Sider/index";
import { Outlet } from "react-router-dom";

const GeneralLayout = () => {
  return (
    <ConfigProvider theme={theme}>
      <Layout style={{ maxHeight: "100vh" }}>
        <Sidebar />
        <Layout>
          <Outlet />
        </Layout>
      </Layout>
    </ConfigProvider>
  );
};
export default GeneralLayout;
