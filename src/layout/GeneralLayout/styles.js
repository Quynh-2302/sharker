import styled from "styled-components";
import { Layout } from "antd";

const PrivateLayoutWrapper = styled(Layout)`
  min-height: 100vh;
  .ant-layout {
    transition: all 0.2s ease 0s;
    /* padding-left: ${({ collapsed }) =>
      collapsed === "true" ? "64px" : "200px"}; */

    @media only screen and (max-width: 640px) {
      padding-left: 0px;
    }
  }
  .ant-anchor-wrapper {
    margin-left: 0px;
    padding-left: 0px;
  }
  .container {
    overflow-y: auto;
    overflow-x: hidden;
    display: flex;
    flex-direction: column;
  }

  .content {
    padding: 36px 24px;
    flex: 1;
    ${
      "" /* @media only screen and (max-width: 640px) {
      padding-top: 80px;
    } */
    }
  }
  .trigger {
    font-size: 20px;
    padding: 5px;
    cursor: pointer;
    transition: color 0.2s;
    margin-right: 10px;
  }
`;

export default PrivateLayoutWrapper;
