import styled from "styled-components";
import "../../styles/global.css";
import { Layout } from "antd";

const Container = styled(Layout)`
  display: flex;
  max-height: 100vh;
  justify-content: center;
  align-items: center;
  padding: 278px 0px 314px 0px;
  background: var(--bg-login-color);

  .inputForm {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 44px 32px;
    background: var(--bg-color);
    gap: 30px;
    border-radius: 20px;
    box-shadow: 0px 4px 24px 0px rgba(0, 0, 0, 0.15);
  }

  .input {
    display: flex;
    width: 286px;
    height: 40px;
    padding: 12px 24px;
    align-items: center;
    gap: 10px;
    flex-shrink: 0;
    border-radius: 5px;
    border: 1px solid var(--boder-input-color);
    /* background: var(--bg-input-color); */
  }

  .btnSignIn {
    display: flex;
    width: 286px;
    height: 42px;
    padding: 8px 16px;
    margin: 0px;
    gap: 8px;
    flex-shrink: 0;
    border-radius: 5px;
    background: var(--primary-color);
    color: var(--bg-color);
    justify-content: center;
    align-items: center;
  }
`;

export default Container;
