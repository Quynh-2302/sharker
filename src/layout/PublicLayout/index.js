import React from "react";
import { ConfigProvider, Form } from "antd";
import { Logo } from "../../assets/logo/icon";
import Container from "./styles";

import { theme } from "../../styles/layout";

const PublicLayout = ({ children }) => {
  const [form] = Form.useForm();
  return (
    <ConfigProvider theme={theme}>
      <Container>
        <Form
          form={form}
          className="inputForm"
          name="basic"
          wrapperCol={{
            span: 24,
          }}
          autoComplete="off"
        >
          <Logo style={{ width: "166px", height: "36px" }} />
          {children}
        </Form>
      </Container>
    </ConfigProvider>
  );
};
export default PublicLayout;
