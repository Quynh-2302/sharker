import { ConfigProvider, Layout } from "antd";
import React from "react";
import "../../styles/global.css";
import { theme } from "../../styles/layout";

const PrivateLayout = ({ children }) => {
  return (
    <ConfigProvider theme={theme}>
      <Layout style={{ maxHeight: "100vh" }}>
        <Layout>{children}</Layout>
      </Layout>
    </ConfigProvider>
  );
};
export default PrivateLayout;
